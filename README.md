# Démonstrateurs et utilitaires du groupe Architectes de la Direction du numérique

Ces projets servent d'illustrations pour les actions engagées par la Direction du
numérique (Dnum) dans la livraison continue et l'orientation cloud.

Les choix de conception faits dans ces démonstrateurs peuvent être simplistes par endroits.
Leurs objectifs est d'illustrer une pratique précise, et non une liste exhaustive de bonnes pratiques.

Vous pouvez participer à ces démonstrateurs par le biais des *issues* ou en vous rapprochant de
l'équipe des architectes de la Dnum.

(*Crédits images : [Heritage Type](https://www.heritagetype.com/pages/free-vintage-illustrations)*)